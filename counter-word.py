#!/usr/bin/python3

import sys

with open(sys.argv[1], "r") as file:
    text = file.read()
    for char in '-.,\n':
            text = text.replace(char,' ')
    word_list = text.split()
    print(len(word_list))
