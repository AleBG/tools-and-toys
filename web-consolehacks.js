/*

These small js codes are meant to be executed in a browser's console,
to manipulate some behavior of a web page

*/


/* ========================
youtube.com
======================== */

/* Control a video's speed
    - Accepts more values than the defaults (e.g. 0.9; 0.8725)
*/
$('video').playbackRate = 0.9;
