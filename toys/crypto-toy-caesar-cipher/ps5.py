import string

### DO NOT MODIFY THIS FUNCTION ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    #print('Loading word list from file...')
    # inFile: file
    in_file = open(file_name, 'r')
    # line: string
    line = in_file.readline()
    # word_list: list of strings
    word_list = line.split()
    #print('  ', len(word_list), 'words loaded.')
    in_file.close()
    return word_list

### DO NOT MODIFY THIS FUNCTION ###
def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list

### DO NOT MODIFY THIS FUNCTION ###
def get_story_string():
    """
    Returns: a joke in encrypted text.
    """
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story

WORDLIST_FILENAME = 'words.txt'


'''
    PROBLEM 1: Complete the Message class
'''
class Message(object):
    ### DO NOT MODIFY THIS METHOD ###
    def __init__(self, text):
        '''
        Initializes a Message object
                
        text (string): the message's text

        a Message object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words
        '''
        self.message_text = text
        self.valid_words = load_words(WORDLIST_FILENAME)

    ### DO NOT MODIFY THIS METHOD ###
    def get_message_text(self):
        '''
        Used to safely access self.message_text outside of the class
        
        Returns: self.message_text
        '''
        return self.message_text

    ### DO NOT MODIFY THIS METHOD ###
    def get_valid_words(self):
        '''
        Used to safely access a copy of self.valid_words outside of the class
        
        Returns: a COPY of self.valid_words
        '''
        return self.valid_words[:]
        
    def build_shift_dict(self, shift):
        '''
        Creates a dictionary that can be used to apply a cipher to a letter.
        The dictionary maps every uppercase and lowercase letter to a
        character shifted down the alphabet by the input shift. The dictionary
        should have 52 keys of all the uppercase letters and all the lowercase
        letters only.        
        
        shift (integer): the amount by which to shift every letter of the 
        alphabet. 0 <= shift < 26

        Returns: a dictionary mapping a letter (string) to 
                 another letter (string). 
        '''
        assert 0 <= shift < 26, "The shift must be between 0 and 25."
        
        lower_alphabet = 'abcdefghijklmnopqrstuvwxyz'
        upper_alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        shift_dict = {}
        
        for l in lower_alphabet:
            shift_dict[l] = lower_alphabet[(lower_alphabet.index(l) + shift) % 26]
            # new letter = `shift` + the index of l 
        for l in upper_alphabet:
            shift_dict[l] = upper_alphabet[(upper_alphabet.index(l) + shift) % 26]
        
        return shift_dict
        

    def apply_shift(self, shift):
        '''
        Applies the Caesar Cipher to self.message_text with the input shift.
        Creates a new string that is self.message_text shifted down the
        alphabet by some number of characters determined by the input shift        
        
        shift (integer): the shift with which to encrypt the message.
        0 <= shift < 26

        Returns: the message text (string) in which every character is shifted
             down the alphabet by the input shift
        '''
        caesar_cipher = self.build_shift_dict(shift)
        encrypted_message = Message("")
        
        for c in self.message_text:
            try:
                c = caesar_cipher[c]
                encrypted_message.message_text += c
            except:
                c = c
                encrypted_message.message_text += c
        
        return encrypted_message
    
'''
    PROBLEM 2: Fill-in PlaintextMessage
'''    

class PlaintextMessage(Message):
    def __init__(self, text, shift):
        '''
        Initializes a PlaintextMessage object        
        
        text (string): the message's text
        shift (integer): the shift associated with this message

        A PlaintextMessage object inherits from Message and has five attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
            self.shift (integer, determined by input shift)
            self.encrypting_dict (dictionary, built using shift)
            self.message_text_encrypted (string, created using shift)

        Hint: consider using the parent class constructor so less 
        code is repeated
        '''
        Message.__init__(self, text)
        self.shift = shift
        self.encrypting_dict = Message.build_shift_dict(self, shift)
        self.message_text_encrypted = Message.apply_shift(self, shift)

    def get_shift(self):
        '''
        Used to safely access self.shift outside of the class
        
        Returns: self.shift
        '''
        return self.shift

    def get_encrypting_dict(self):
        '''
        Used to safely access a copy self.encrypting_dict outside of the class
        
        Returns: a COPY of self.encrypting_dict
        '''
        return self.encrypting_dict.copy()

    def get_message_text_encrypted(self):
        '''
        Used to safely access self.message_text_encrypted outside of the class
        
        Returns: self.message_text_encrypted
        '''
        return self.message_text_encrypted.get_message_text()

    def change_shift(self, shift):
        '''
        Changes self.shift of the PlaintextMessage and updates other 
        attributes determined by shift (ie. self.encrypting_dict and 
        message_text_encrypted).
        
        shift (integer): the new shift that should be associated with this message.
        0 <= shift < 26

        Returns: nothing
        '''
        assert 0 <= shift < 26, "The shift must be between 0 and 25."
        self = PlaintextMessage.__init__(self, self.get_message_text(), shift)

'''
    PROBLEM 3: Fill-in CiphertextMessage
'''
     
class CiphertextMessage(Message):
    def __init__(self, text):
        '''
        Initializes a CiphertextMessage object
                
        text (string): the message's text

        a CiphertextMessage object has two attributes:
            self.message_text (string, determined by input text)
            self.valid_words (list, determined using helper function load_words)
        '''
        Message.__init__(self, text)

    def decrypt_message(self):
        '''
        Decrypt self.message_text by trying every possible shift value
        and find the "best" one. We will define "best" as the shift that
        creates the maximum number of real words when we use apply_shift(shift)
        on the message text. If s is the original shift value used to encrypt
        the message, then we would expect 26 - s to be the best shift value 
        for decrypting it.

        Note: if multiple shifts are equally good such that they all create 
        the maximum number of you may choose any of those shifts (and their
        corresponding decrypted messages) to return

        Returns: a tuple of the best shift value used to decrypt the message
        and the decrypted message text using that shift value
        '''
        #text = self.get_message_text()
        #words = text.split(" ")
        keys_and_max_fitness_messages = []  # Will be a list of tuples
        candidates_fitness = {}
        candidates_messages = {}
        
        for k in range(0, 26):
            k_fitness = 0
            k_candidate = self.apply_shift(k)
            candidates_messages[k] = k_candidate
            
            k_words = k_candidate.get_message_text().split(" ")
            for w in k_words:
                if is_word(self.valid_words, w) == True:
                    k_fitness += 1
            candidates_fitness[k] = k_fitness
        
        for k in candidates_fitness:
            if candidates_fitness[k] == max(candidates_fitness.values()):
                keys_and_max_fitness_messages.append((k, candidates_messages[k].get_message_text()))
        
        return tuple(keys_and_max_fitness_messages)
    
'''
    PROBLEM 4: Decrypt a story
'''
def decrypt_story(story):
    ''' IN: a string of caesar-encrypted text
        OUT: prints human-readable text in English
        
        CiphertextMessage.decrypt_message() returns a tuple containing tuples (k, m): 
            where k is the key used for decryption
            where m is one of the best decrypted messages, given the list of valid English words
        We have thus to access each nested tuple's message using `dec_story[index][1]`
    '''
    enc_story = CiphertextMessage(story)
    dec_story = enc_story.decrypt_message()
    for i in range(len(dec_story)):
        print("Best decrypted message {}:\n{}\n".format((i+1), dec_story[i][1]))

'''
    TEST
'''
print("Testing the Classes and Functions...\n")

print("Example test case: PlaintextMessage('hello', 2)  <-- Of the form (message, encryption_key)")
plaintext = PlaintextMessage('hello', 2)
print('Expected Output: jgnnq')
print('Actual Output:', plaintext.get_message_text_encrypted(),"\n")
    
print("Example test case: CiphertextMessage('jgnnq')")
ciphertext = CiphertextMessage('jgnnq')
print('Expected Output:', (24, 'hello'), "  <-- Of the form (decryption_key, message)")
print('Actual Output:', ciphertext.decrypt_message(),"\n")

print("Example test case: decrypt_story(story.txt)  <-- An encrypted story")
story1 = get_story_string()
print(story1, "\n")
decrypt_story(story1)





