In this problem set, you will use regression analysis to model the climate of different areas and try to find evidence of global warming.
You will create models to analyze and visualize climate change in terms of temperature.

For this problem set, we will use temperature data obtained from the National Centers for Environmental Information (NCEI).
The data, stored in `data.csv` contains the average temperatures observed in 21 US cities from 1961 to 2015.

In order to parse the raw data, in `ps4.py` we have implemented a helper class `Climate`.
You can initialize an instance of the Climate class by providing the filename of the raw data.
