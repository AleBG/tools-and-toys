def genTest():
    yield 1
    yield 2

def genFib():
    # Base cases
    fibn_1 = 1
    fibn_2 = 0
    
    # Fib function for n > 1: fib(n) = fib(n-1) + fib(n-2)
    while True:
        next = fibn_1 + fibn_2
        yield next
        fibn_2 = fibn_1
        fibn_1 = next

