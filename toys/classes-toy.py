class Coordinate(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def distance(self, other):
        x_diff_sq = (self.x-other.x) ** 2
        y_diff_sq = (self.y-other.y) ** 2
        return (x_diff_sq + y_diff_sq) ** 0.5
    def __str__(self):
        return "< " + str(self.x) + ", " + str(self.y) + " >"

class Clock(object):
    def __init__(self, time):
        self.time = time
    def print_time(self):
        print(self.time)
        print("abc")

class lista(object):
    def __init__(self):
        self.vals = []
    def insert(self, e):
        self.vals.append(e)
